let input = document.getElementById("input");
let btn = document.getElementById("button");
let lists = document.getElementById("lists");
let clearAll = document.getElementById("clearAll");
let complete = document.getElementById("complete");
let all = document.getElementById("all");
let active = document.getElementById("active");
let clearCompleted = document.getElementById("clearCompleted");


function saveInLocalStorage() {
  lists.childNodes.forEach (list => {
    localStorage.setItem("list", list.childNodes[1].textContent)
  })
}

function createList() {
  let li = document.createElement("li");
  let close = document.createElement("button");
  let checkBox = document.createElement("input");
  let span = document.createElement("span");
  span.textContent = input.value.trim();
  checkBox.type = "checkbox";
  li.appendChild(checkBox);
  li.appendChild(span);
  li.dataset.status = "incomplete";
  li.className = "block";
  close.innerText = "delete";
  li.appendChild(close);
  lists.appendChild(li);
  li.addEventListener("dblclick", () => {
    li.childNodes[1].contentEditable = true;
  });
  document.addEventListener("click", (e) => {
    if (!document.getElementsByClassName("container")[0].contains(e.target)) {
      li.childNodes[1].contentEditable = false;
    }
  });
  console.log(li);
  return { close, checkBox };
}

function clearAllCallBack() {
  while (lists.firstElementChild) {
    lists.removeChild(lists.lastElementChild);
    clearAll.classList.add("none");
  }
}

function completeCallBack() {
  lists.childNodes.forEach((list) => {
    list.dataset.status == "incomplete"
      ? list.classList.replace("block", "none")
      : list.classList.replace("none", "block");
  });
}

function activeCallBack() {
  lists.childNodes.forEach((list) => {
    list.dataset.status == "complete"
      ? list.classList.replace("block", "none")
      : list.classList.replace("none", "block");
  });
}

function allCallBack() {
  lists.childNodes.forEach((list) => {
    if (list.className == "none") {
      list.classList.remove("none");
    }
  });
}

function clearCompletedCallBack() {
  Array.from(lists.childNodes).forEach((list) => {
    if (list.dataset.status == "complete") {
      lists.removeChild(list);
    }
  });
}




input.addEventListener("keydown", (e) => {
  if (e.key === "Enter" && input.value !== "") {
    let { close, checkBox } = createList();
    input.value = "";
    input.focus();
    clearAll.classList.remove("none");
    saveInLocalStorage()
    close.addEventListener("click", (e) => {
      lists.removeChild(e.target.parentNode);
      if (lists.childNodes.length == 0) {
        clearAll.classList.add ("none")
      }
    });
    checkBox.addEventListener("change", (e) => {
      if (checkBox.checked) {
        e.target.parentNode.dataset.status = "complete";
        e.target.parentNode.style.textDecoration = "line-through";
      } else {
        e.target.parentNode.dataset.status = "incomplete";
        e.target.parentNode.style.textDecoration = "none";
      }
    });
  }
});

complete.addEventListener("click", completeCallBack);

active.addEventListener("click", activeCallBack);

all.addEventListener("click", allCallBack);

clearCompleted.addEventListener("click", clearCompletedCallBack);

clearAll.addEventListener("click", clearAllCallBack);
